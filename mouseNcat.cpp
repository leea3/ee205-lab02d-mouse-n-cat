///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Arthur Lee <leea3@hawaii.edu>
/// @date    24 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {

   /* with no parameters, argc = 1 */
   if(argc != 1){

      /* printing argc[0] prints the command to run code */
      for(int words_in_string = argc - 1; words_in_string >= 0; words_in_string--){

         std::cout << argv[words_in_string] << std::endl;

      }
   }
   else{
      int i = 0;

      /* type "printenv" in bash to double check if the output is correct */
      while(envp[i] != NULL){

         std::cout << envp[i] << std::endl;
         i++;

      }
   }

   std::exit( EXIT_SUCCESS );
}
