###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02d - Mouse n Cat - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Makefile for  Mouse n Cat Lab
###
### @author  Arthur Lee <leea3@hawaii.edu>
### @date    24 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

#gcc for C, g++ for C++
CC     = g++
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

#.c for C, .cpp for C++
mouseNcat: mouseNcat.cpp
	$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.cpp

clean:
	rm -f $(TARGET) *.o

